// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);
// The "get" method returns the value of the key passed in as an argument
let userId = params.get('userId');

let token = localStorage.getItem('token');

let passwordResetForm = document.querySelector("#passwordReset");


passwordResetForm.addEventListener("submit", (e) => { 

	e.preventDefault();

	let currentPassword = document.querySelector("#currentPassword").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	let userPassword = password1;


	if ((password1 === password2) && (password1 !== '' && password2 !== '') && (currentPassword !== '')) {

		fetch('https://limitless-wildwood-79305.herokuapp.com/api/users/comparePassword', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userId: userId,      // from line 4
				password: currentPassword // first values from controller/route, second values declaration in lines 45 to 49
		    })
		})
		.then(res => res.json())
		.then(data => { 

			if (data === true) {

				fetch('https://limitless-wildwood-79305.herokuapp.com/api/users/resetpassword', {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
				        'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						userId: userId,      // from line 4
						password: userPassword // first values from controller/route, second values declaration in lines 45 to 49
				    })
				})
				.then(res => res.json())
				.then(data => { 

				    console.log(data);

				    if(data === true){

					   // Update course successful
					   // Redirect to courses page
					    alert("Your password has been reset!")
					    window.location.replace("./profile.html");

				    } else{

						// Error in updating a course
						alert("something went wrong");

				    }
				})

			} else {

				alert("Current Password is Incorrect")
			}
		})

	} else {

		alert("Please ensure that the input fields are properly filled out")
	
	}
			
})
	