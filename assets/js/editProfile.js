// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);
// The "get" method returns the value of the key passed in as an argument
let userId = params.get('userId');

let token = localStorage.getItem('token');

let editForm = document.querySelector("#editUser");


	let firstName = document.querySelector("#firstName");
	let lastName = document.querySelector("#lastName");
	let mobileNumber = document.querySelector("#mobileNumber");
	let email = document.querySelector("#userEmail");

	

	fetch(`https://limitless-wildwood-79305.herokuapp.com/api/users/${userId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);


			// Assign the retrieved data as placeholders and the current value of the input fields
			firstName.placeholder = data.firstName;
			lastName.placeholder = data.lastName;
			mobileNumber.placeholder = data.mobileNo;
			email.placeholder = data.email;
			firstName.value = data.firstName;
			lastName.value = data.lastName;
			mobileNumber.value = data.mobileNo;
			email.value = data.email;
			
		})
		
		// add an event to the register form
		editForm.addEventListener("submit", (e) => { 

			e.preventDefault();

			let userFirstName = firstName.value;
			let userLastName = lastName.value;
			let userMobileNo = mobileNumber.value;    // second values declared in lines 11-16
			let userEmail = email.value;

			if (userMobileNo.length === 11){

			fetch('https://limitless-wildwood-79305.herokuapp.com/api/users', {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
            		'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
		            userId: userId,      // from line 4
		            firstName: userFirstName,    
		            lastName: userLastName, 		
		            mobileNo: userMobileNo,     // first values from controller/route, second values declaration in lines 45 to 49
		            email: userEmail
        		})
			})
			.then(res => res.json())
    		.then(data => { 

    			console.log(data);

    			if(data === true){

	    			// Update course successful
	    	    	// Redirect to courses page
	    	    	alert("User Information Successfully Updated!")
	    	    	window.location.replace("./profile.html");

    			} else{

		    	    // Error in updating a course
		    	    alert("something went wrong");

    			}

    		})
		} else {

			alert("Please verify mobile number input")
		}	
	})