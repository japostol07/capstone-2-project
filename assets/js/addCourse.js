let formSubmit = document.querySelector("#createCourse");

let token = localStorage.getItem("token");

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	fetch('https://limitless-wildwood-79305.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data === true){

			alert("Course successfully added!");
			window.location.replace("./courses.html");

		} else {

			alert("Something went wrong.");

		}

	})

})