let adminUse = localStorage.getItem("isAdmin");
let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
let profileBtn = document.querySelector("#profileBtn");

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {     // not logged on

	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/login.html" class="nav-link"> Log In </a>
			</li>
		`

	registerBtn.innerHTML =
		`
			<li class="nav-item">
				<a href="./pages/register.html" class="nav-link"> Register </a>
			</li>
		`

} else {

	if (adminUse == "false"){

		navItems.innerHTML = 
			`
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
			`

		profileBtn.innerHTML = 
			`
			<li class="nav-item">
				<a href="./pages/profile.html" class="nav-link"> Profile </a>
			</li>
			`
			
	} else {

		navItems.innerHTML = 
			`
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
			`

	}
}