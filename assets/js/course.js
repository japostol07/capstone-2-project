let adminUser = localStorage.getItem("isAdmin");

// "window.location.search" returns the query string part of the URL
console.log(window.location.search);

// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "has" method checks if the "courseId" key exists in the URL query string
// The method returns true if the key exists
console.log(params.has('courseId'));

// The "get" method returns the value of the key passed in as an argument
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let returnBtn = document.querySelector("#returnBtn") // return button
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

returnBtn.innerHTML =
`
<a href="./courses.html" class="btn btn-success">
	Return
</a>

`

fetch(`https://limitless-wildwood-79305.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {                    // Get details of specific course

	console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;

	if(adminUser == "false" || !adminUser){

		enrollContainer.innerHTML = 
			`
				<button id="enrollButton" class="btn btn-block btn-primary">
					Enroll
				</button>
			`

		document.querySelector("#enrollButton").addEventListener("click", () => {

			fetch('https://limitless-wildwood-79305.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data);

				if(data === true){

					alert("Thank you for enrolling! See you in class!");
					window.location.replace("./courses.html");

				} else {

					alert("Please Login");
					window.location.replace("./login.html");

				}

			}) 

		})  

	} else {  

		enrollContainer.innerHTML = 

			`
			<table class="table">
			 <h5 class="text-Center">Enrollees</h5>
			 	<thead>
					<th>Name</th>
					<th>Enrollment Date</th> 
			 	</thead>
			 	<tbody id="enrolleeList">
				</tbody>
			</table
			`
		
		data.enrollees.map(userData => {

			console.log(userData);

			fetch(`https://limitless-wildwood-79305.herokuapp.com/api/users/${userData.userId}`)
			.then(res => res.json())
			.then(data => {

				enrolleeList.innerHTML += 

				   `
				    <tr>
						<td>${data.firstName} ${data.lastName}</td>
						<td>${userData.enrolledOn}</td>
					 </tr>

					`

			})
		})


	}   

}) 