let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton")  // Add course button
let Token = localStorage.getItem("token");

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

} else {

	modalButton.innerHTML =
		`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-secondary">Add Course</a>
		</div>

		`
}

fetch('https://limitless-wildwood-79305.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {            // Show all courses

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

		if (data.length < 1) {

			courseData = "No courses available"
				
		} else {

			courseData = data.map(course => {       //.map

				if(adminUser == "false" || !adminUser){

					cardFooter =
						`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class= "btn btn-secondary text-white btn-block editButton">
							Select Course
						</a>
						`

				} else {

					if (course.isActive == true){

						cardFooter =
							`
							<a href="./course.html?courseId=${course._id}" value="${course._id}" class= "btn btn-success text-white btn-block editButton">
								View Enrollees
							</a>
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class= "btn btn-primary text-white btn-block editButton">
								Edit Course
							</a>
					
							<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class= "btn btn-danger text-white btn-block deleteButton">
								Archive Course
							</a>
							`
					} else {

						cardFooter =
							`
							<a href="./course.html?courseId=${course._id}" value="${course._id}" class= "btn btn-success text-white btn-block editButton">
								View Enrollees
							</a>
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class= "btn btn-primary text-white btn-block editButton">
								Edit Course
							</a>
					
							<a href="./unDeleteCourse.html?courseId=${course._id}" value="${course._id}" class= "btn btn-warning text-white btn-block deleteButton">
								Unarchive Course
							</a>
							`
						}
					}

		if ((!Token) && (course.isActive == false)){    // Inactive courses will not show up to users who are not yet registered.

			course = null;
		
		} else if ((adminUser == "false") && (course.isActive == false)){    // Inactive courses will not show up to regular user accounts

			course = null;

		} else {

			return (
				`
		        <div class="col-md-6 my-3">
			        <div class = "card">
						<div class= "card-body">
							<h5 class=	"card-title">
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								₱ ${course.price}
							</p>
							<div class = "card-footer">
								${cardFooter}
							</div>
			 			</div>
					</div>
				</div>

				`
			)
		}

	}).join("");  //replaces the commas (result of .map) in the array with an empty string

}

	document.querySelector("#coursesContainer").innerHTML = courseData   // to set the contents of courseData to the Container. Check courses.html

})