let token = localStorage.getItem("token");
// console.log(token);
let returnBtn = document.querySelector("#returnBtn") // return button
let profileContainer = document.querySelector("#profileContainer");
let editButton = document.querySelector("#editButton")   // Edit/Update profile button
let passwordResetButton = document.querySelector("#passwordResetButton")   // Password reset button

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

	fetch('https://limitless-wildwood-79305.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
	})
	.then(res => res.json())
	.then(data => {                     // Get details of specific user

		console.log(data);

		editButton.innerHTML = 
		`
		<a href="./editProfile.html?userId=${data._id}" value="${data._id}" class= "btn btn-secondary text-white editButton w-25">
			Edit Profile
		</a>
		`

		passwordResetButton.innerHTML = 
		`
		<a href="./passwordReset.html?userId=${data._id}" value="${data._id}" class= "btn btn-secondary text-white editButton w-25">
			Reset Password
		</a>
		`

		returnBtn.innerHTML =
		`
		<div class="row justify-content-center">
		<a href="./courses.html" class="btn btn-secondary">
			Return
		</a>
		</div>
		`

		profileContainer.innerHTML = 
			`
				<div class="col-md-12">
					<section class="jumbotron my-5">		
						<h3 class="text-center">Name: ${data.firstName} ${data.lastName}</h3>
						<h3 class="text-center">Email: ${data.email}</h3>
						<h3 class="text-center">Contact # : ${data.mobileNo}</h3>
						<h3 class="text-center mt-5">Courses </h3>
						<table class="table">
							<thead>
								<tr>
									<th> Course Name </th>
									<th> Enrolled On </th>
									<th> Status </th>
								</tr>
							</thead>
							<tbody id="courses">
							</tbody>
						</table> 
					</section>
				</div>
			`

		let courses = document.querySelector("#courses");

		data.enrollments.map(courseData => {       // Data from line 23

			console.log(courseData);

			fetch(`https://limitless-wildwood-79305.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				courses.innerHTML +=      // +
					`<tr>
						<td>${data.name}</td>
						<td>${courseData.enrolledOn}</td>
						<td>${courseData.status}</td>
					</tr>
					`

			})
		
		})

	})
}